from django.urls import path
from app.views import my_app_page
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns = [
    path('', my_app_page),
]
urlpatterns += staticfiles_urlpatterns()