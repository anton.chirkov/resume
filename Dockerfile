#syntax=docker/dockerfile:1
FROM python:3.10.11-alpine
WORKDIR /app
COPY . .
RUN pip3 install --no-cache-dir -r /app/requirements.txt
RUN python3 manage.py migrate
EXPOSE 8000/tcp
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "project.wsgi"]