## Getting started [ENG]

## Clone repository
```
git clone git@gitlab.com:anton.chirkov/resume.git
```
## Run wichout docker

Tested on ubuntu 22.04 and Linux mint 21.1
```
pip3 install -r requirements.txt
python3 manage.py runserver
```

## Run in docker-compose

First of all we need build image:
```
sudo docker-compose build
```
Now need run migration:
```
sudo docker run --rm  registry.gitlab.com/anton.chirkov/resume python3 manage.py migrate
```
And after migration run docker-compose:
```
sudo docker-compose up
```
If you need run in background:
```
sudo docker-compose up -d
```

## Gitlab CI

This repository have .gitlab-ci.yml file. This file include .gitlab/main.yml, where we find build, push, deploy stages.

## Nomad config example

Also in repository in file nomad.job i write example config for harsicorp nomad. If you want use him - set dc name and other information for your cluster.

## Начнем работу с репозиторием [RU]

## Клонируем код
```
git clone git@gitlab.com:anton.chirkov/resume.git
```
## Как запустить без docker

Проверено на ubuntu 22.04 и Linux mint 21.1
```
pip3 install -r requirements.txt
python3 manage.py runserver
```

## Запуск в docker-compose

Сначала нужно запустить build:
```
sudo docker-compose build
```
После этого выполнить миграции:
```
sudo docker run --rm  registry.gitlab.com/anton.chirkov/resume python3 manage.py migrate
```
Теперь запустим docker-compose:
```
sudo docker-compose up
```
Если нужно запустить в фоне:
```
sudo docker-compose up -d
```

## Gitlab CI

В этом репозитории есть файл .gitlab-ci.yml. Он включает файл .gitlab/main.yml, где можно увидеть сборку, отправку в registry и деплой.

## Nomad config example

Так же в этом репозитории есть файл nomad.job. Я написал пример конфига для harsicorp nomad. Если есть желание его использовать - нужно установить имя dc и прочую информацию вашего кластера.
